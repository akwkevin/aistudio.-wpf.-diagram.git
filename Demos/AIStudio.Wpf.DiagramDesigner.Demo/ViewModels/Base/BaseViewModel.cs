﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AIStudio.Wpf.DiagramDesigner.Demo.ViewModels
{
    class BaseViewModel : BindableBase
    {
        public DiagramViewModel DiagramViewModel
        {
            get; set;
        }

        public string Title
        {
            get;set;
        }

        public string Info
        {
            get; set;
        }
    }
}
