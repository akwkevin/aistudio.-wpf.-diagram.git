﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Shapes;

namespace AIStudio.Wpf.DiagramDesigner.Demo.ViewModels
{
    class LockedViewModel : BaseViewModel
    {
        public LockedViewModel()
        {
            Title = "Locked models";
            Info = "All the nodes and links in this example are locked, they cannot be moved nor deleted." +
                    //"All the TOP ports are locked as well, so you can't create links from/to them, but you can from/to the others." +
                    "Newly created links aren't locked, so they can be deleted.";

            DiagramViewModel = new DiagramViewModel();
            DiagramViewModel.DiagramOption.LayoutOption.CellHorizontalAlignment = CellHorizontalAlignment.Center;
            DiagramViewModel.DiagramOption.LayoutOption.CellVerticalAlignment = CellVerticalAlignment.Center;
            DiagramViewModel.DiagramOption.LayoutOption.PageSizeType = PageSizeType.Custom;
            DiagramViewModel.DiagramOption.LayoutOption.PageSize = new Size(double.NaN, double.NaN);
            DiagramViewModel.ColorViewModel = new ColorViewModel();
            DiagramViewModel.ColorViewModel.FillColor.Color = System.Windows.Media.Colors.Orange;

            DefaultDesignerItemViewModel node1 = new DefaultDesignerItemViewModel(DiagramViewModel) { Left = 50, Top = 50, Text = "1" };
            DiagramViewModel.Add(node1);

            DefaultDesignerItemViewModel node2 = new DefaultDesignerItemViewModel(DiagramViewModel) { Left = 300, Top = 300, Text = "2" };
            DiagramViewModel.Add(node2);

            DefaultDesignerItemViewModel node3 = new DefaultDesignerItemViewModel(DiagramViewModel) { Left = 300, Top = 50, Text = "3" };
            DiagramViewModel.Add(node3);

            ConnectionViewModel connector1 = new ConnectionViewModel(DiagramViewModel, node1.RightConnector, node2.LeftConnector, DrawMode.ConnectingLineSmooth, RouterMode.RouterNormal);
            DiagramViewModel.Add(connector1);

            ConnectionViewModel connector2 = new ConnectionViewModel(DiagramViewModel, node2.RightConnector, node3.RightConnector, DrawMode.ConnectingLineStraight, RouterMode.RouterOrthogonal);
            DiagramViewModel.Add(connector2);

            //node1.LockInfo.LockObject.FirstOrDefault(p => p.LockFlag == LockFlag.All).IsChecked = true;//这个也可以
            node1.IsReadOnly = true;
            //node2.LockInfo.LockObject.FirstOrDefault(p => p.LockFlag == LockFlag.All).IsChecked = true;//这个也可以
            node2.IsReadOnly = true;
            //node3.LockInfo.LockObject.FirstOrDefault(p => p.LockFlag == LockFlag.All).IsChecked = true;//这个也可以
            node3.IsReadOnly = true;
            //link1.LockInfo.LockObject.FirstOrDefault(p => p.LockFlag == LockFlag.All).IsChecked = true;//这个也可以
            connector1.IsReadOnly = true;
            // link2.LockInfo.LockObject.FirstOrDefault(p => p.LockFlag == LockFlag.All).IsChecked = true;//这个也可以
            connector2.IsReadOnly = true;
        }
    }
}
