﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AIStudio.Wpf.Flowchart.ViewModels;

namespace AIStudio.Wpf.Flowchart.Controls
{
    /// <summary>
    /// Interaction logic for FlowchartToolBoxControl.xaml
    /// </summary>
    public partial class FlowchartToolBoxControl : UserControl
    {
        public FlowchartToolBoxControl()
        {
            InitializeComponent();
            this.DataContext = new FlowchartToolBoxViewModel();
        }
    }
}
