﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AIStudio.Wpf.Mind.Controls
{
    /// <summary>
    /// RemarkControl.xaml 的交互逻辑
    /// </summary>
    public class RemarkControl : Control
    {
        static RemarkControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(RemarkControl), new FrameworkPropertyMetadata(typeof(RemarkControl)));
        }

        /// <summary>Identifies the <see cref="Remark"/> dependency property.</summary>
        public static readonly DependencyProperty RemarkProperty
            = DependencyProperty.Register(nameof(Remark), typeof(string), typeof(RemarkControl));

        /// <summary> 
        /// Whether or not the "popup" menu for this control is currently open
        /// </summary>
        public string Remark
        {
            get => (string)this.GetValue(RemarkProperty);
            set => this.SetValue(RemarkProperty, (string)value);
        }
    }
}
