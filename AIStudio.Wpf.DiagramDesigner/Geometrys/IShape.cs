﻿using System.Collections.Generic;

namespace AIStudio.Wpf.DiagramDesigner.Geometrys
{
    public interface IShape
    {
        IEnumerable<PointBase> GetIntersectionsWithLine(LineBase line);
    }
}
