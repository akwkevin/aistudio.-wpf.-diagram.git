﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AIStudio.Wpf.DiagramDesigner.Algorithms
{
    public static class LinksReconnectionAlgorithms
    {
        public static void ReconnectLinksToClosestPorts(this IDiagramViewModel diagram)
        {
            // Only refresh ports once
            //var portsToRefresh = new HashSet<FullyCreatedConnectorInfo>();

            foreach (var link in diagram.Items.OfType<ConnectionViewModel>())
            {
                if (link.IsFullConnection == false)
                    continue;

                var sourcePorts = link.SourceConnectorInfoFully.DataItem.Connectors;
                var targetPorts = link.SinkConnectorInfoFully.DataItem.Connectors;

                // Find the ports with minimal distance
                var minDistance = double.MaxValue;
                var minSourcePort = link.SourceConnectorInfoFully;
                var minTargetPort = link.SinkConnectorInfoFully;
                foreach (var sourcePort in sourcePorts)
                {
                    foreach (var targetPort in targetPorts)
                    {
                        var distance = sourcePort.Position.DistanceTo(targetPort.Position);
                        if (distance < minDistance)
                        {
                            minDistance = distance;
                            minSourcePort = sourcePort;
                            minTargetPort = targetPort;
                        }
                    }
                }

                // Reconnect
                if (link.SourceConnectorInfo != minSourcePort)
                {
                    //portsToRefresh.Add(link.SourceConnectorInfoFully);
                    //portsToRefresh.Add(minSourcePort);
                    link.SourceConnectorInfo = minSourcePort;
                }

                if (link.SinkConnectorInfo != minTargetPort)
                {
                    //portsToRefresh.Add(link.SinkConnectorInfoFully);
                    //portsToRefresh.Add(minTargetPort);
                    link.SinkConnectorInfo = minTargetPort;
                }
            }

            //foreach (var port in portsToRefresh)
            //    port.Refresh();
        }
    }
}
