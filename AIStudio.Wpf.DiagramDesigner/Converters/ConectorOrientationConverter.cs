﻿using AIStudio.Wpf.DiagramDesigner;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace AIStudio.Wpf.DiagramDesigner
{
    public class ConectorOrientationConverter : MarkupExtension, IValueConverter
    {
        public double Parameter { get; set; } = 5;

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is string)
            {
                string str = value as string;

                var length = 0 - Parameter - (str.Length * 4);
                return length;
            }
            else if (value is double)
            {
                return -Parameter - (double)value;
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public override object ProvideValue(System.IServiceProvider serviceProvider)
        {
            return new ConectorOrientationConverter
            {
                Parameter = this.Parameter,
            };
        }
    }
}
