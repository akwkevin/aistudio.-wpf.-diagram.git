﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AIStudio.Wpf.DiagramDesigner
{
    public enum HorizontalVerticalAlignment
    {
        TopAlignLeft,
        TopAlignCenter,
        TopAlignRight,
        TopAlignJustify,
        CenterAlignLeft,
        CenterAlignCenter,
        CenterAlignRight,
        CenterAlignJustify,
        BottomAlignLeft,
        BottomAlignCenter,
        BottomAlignRight,
        BottomAlignJustify,
    }
}
