﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;
using AIStudio.Wpf.DiagramDesigner.Models;

namespace AIStudio.Wpf.DiagramDesigner
{
    public class MediaItemViewModel : DesignerItemViewModelBase
    {
        protected virtual string Filter { get; set; } = "媒体·|*.*";

        public MediaItemViewModel() : this(null)
        {
 
        }

        public MediaItemViewModel(IDiagramViewModel root) : base(root)
        {

        }

        public MediaItemViewModel(IDiagramViewModel root, SelectableItemBase designer) : base(root, designer)
        {

        }

        public MediaItemViewModel(IDiagramViewModel root, SerializableItem serializableItem, string serializableType) : base(root, serializableItem, serializableType)
        {

        }

        public override SelectableItemBase GetSerializableObject()
        {
            return new MediaDesignerItem(this);
        }

        public override void Init(IDiagramViewModel root, bool initNew)
        {
            base.Init(root, initNew);

            BuildMenuOptions();
        }

        protected override void InitNew()
        {
            base.InitNew();
        }

        protected override void LoadDesignerItemViewModel(SelectableItemBase designerbase)
        {
            base.LoadDesignerItemViewModel(designerbase);

            if (designerbase is MediaDesignerItem designer)
            {
                this.Icon = designer.Icon;
            }
        }

        private void BuildMenuOptions()
        {
            menuOptions = new ObservableCollection<CinchMenuItem>();
            CinchMenuItem menuItem = new CinchMenuItem();
            menuItem.Text = "更换";
            menuItem.Command = MenuItemCommand;
            menuItem.CommandParameter = menuItem;
            menuOptions.Add(menuItem);
        }

        private ICommand _menuItemCommand;
        public ICommand MenuItemCommand
        {
            get
            {
                return this._menuItemCommand ?? (this._menuItemCommand = new SimpleCommand(Command_Enable, ExecuteMenuItemCommand));
            }
        }

        private void ExecuteMenuItemCommand(object obj)
        {
            EditData();
        }

        public override bool Verify()
        {
            if (string.IsNullOrEmpty(Icon))
                return EditData();
            return true;
        }
        public override bool EditData()
        {
            Microsoft.Win32.OpenFileDialog openFile = new Microsoft.Win32.OpenFileDialog();
            openFile.Filter = Filter;

            if (openFile.ShowDialog() == true)
            {
                Icon = openFile.FileName;
                return true;
            }

            return false;
        }

        protected override void ExecuteEditCommand(object param)
        {
        
        }
    }
}
