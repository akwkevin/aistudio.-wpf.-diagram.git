﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace AIStudio.Wpf.DiagramDesigner
{
    public interface ILockObjectViewModel
    {
        List<LockObject> LockObject { get; set; }
        void SetValue(LockObject obj);
        event PropertyChangedEventHandler PropertyChanged;
    }
}
